#!/bin/bash

# Check if a CSV file is provided as an argument
if [ $# -ne 1 ]; then
    echo "Usage: $0 <csv_file>"
    exit 1
fi

# Run the Gnuplot script with the CSV file as an argument
gnuplot -e "input_csv='$1'" generate_chart.gp
