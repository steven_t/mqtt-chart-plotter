import os
import datetime
import subprocess
import config

mqtt_device = config.mqtt_device
mqtt_topic = config.mqtt_topic
mqtt_broker = config.mqtt_broker
output_directory = config.output_directory

# Create the output directory if it doesn't exist
os.makedirs(output_directory, exist_ok=True)

def get_current_date():
    return datetime.datetime.now().strftime("%Y-%m-%d")

def subscribe_and_save():
    current_date = get_current_date()
    filename = os.path.join(output_directory, f"{current_date}_" + mqtt_device + ".json")
    
    with open(filename, "a") as file:
        try:
            process = subprocess.Popen(
                ["mosquitto_sub", "-h", mqtt_broker, "-t", mqtt_topic],
                stdout=subprocess.PIPE,
                text=True
            )
            
            for line in process.stdout:
                # Check if the date has changed
                new_date = get_current_date()
                if new_date != current_date:
                    file.close()
                    current_date = new_date
                    filename = os.path.join(output_directory, f"{current_date}_" + mqtt_device + ".json")
                    file = open(filename, "a")

                file.write(line)
                file.flush()
                
        except KeyboardInterrupt:
            process.terminate()

if __name__ == "__main__":
    subscribe_and_save()
