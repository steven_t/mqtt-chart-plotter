# MQTT Chart Plotter
Listener of certain broker/topic, saving it to file, then manually converting json to csv then plotting it via gnuplot.
Listener creates new file for each day. **Tip: time of host which runs the script.**

For now, its for tasmota and three ds18b20 connected one common gpio. Keep it in mind when sth won't work.


## Installation
1. Probably have to create dirs manually:
- `mkdir log`
- `mkdir charts`

## How to:
1. rename and configure `config_template.py` to `config.py`.
2. edit `listener.py` for your purposses.
3. start `python3 listener.py` and wait to get some records to file.
4. convert json to csv with `json2csv.py log/*.json`
5. edit `generate_chart.gp` for your purposses
6. plot chart `./plot.sh log/*.csv`
7. preview `charts/*.png`

## TO DO:
- [ ] systemd (?) serivce to keep script running all the time  
- [ ] autoconvert json to csv
- [ ] autoplot chart 
- [ ] use specified time then offest it or whatever.
