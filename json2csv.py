import json
import csv
import sys
import os

# Check if the correct number of arguments are provided
if len(sys.argv) != 2:
    print("Usage: python script_name.py input.json")
    sys.exit(1)

# Get the input JSON file name from the command-line argument
input_file = sys.argv[1]

# Check if the input file exists
if not os.path.exists(input_file):
    print(f"Input file '{input_file}' not found.")
    sys.exit(1)

# Generate the output CSV file name based on the input file name
output_file = os.path.splitext(input_file)[0] + '.csv'

# Read the JSON entries from the input file
with open(input_file) as f:
    data = f.readlines()

# Define the CSV header and open the CSV file for writing
csv_header = ['DATE', 'TIME', 'SENSOR 1 TEMP', 'SENSOR 2 TEMP', 'SENSOR 3 TEMP']
with open(output_file, 'w', newline='') as csvfile:
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(csv_header)

    # Process each JSON entry and write to CSV
    for line in data:
        json_entry = json.loads(line)
        date = json_entry['Time'].split('T')[0]
        time = json_entry['Time'].split('T')[1]
        sensor_1_temp = json_entry['DS18B20-1']['Temperature']
        sensor_2_temp = json_entry['DS18B20-2']['Temperature']
        sensor_3_temp = json_entry['DS18B20-3']['Temperature']
        csvwriter.writerow([date, time, sensor_1_temp, sensor_2_temp, sensor_3_temp])

print(f"CSV conversion complete. Output saved to '{output_file}'.")
