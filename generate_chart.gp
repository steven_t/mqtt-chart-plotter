#!/usr/bin/gnuplot
csv_file = input_csv
filename = system("basename ".csv_file)
filename_without_extension = system("basename -s .csv ".csv_file)
output_filename = "charts/".filename_without_extension.".png"

set datafile separator ','
set terminal png size 1200,600
set output output_filename
set xdata time
set timefmt '%H:%M:%S'
set format x '%H:%M'
set xlabel 'Time'
set ylabel 'Temperature (°C)'
set title 'Temperature Data for '.filename_without_extension
plot csv_file using 2:3 with lines smooth bezier title 'Sensor 1', \
     '' using 2:4 with lines smooth bezier title 'Sensor 2', \
     '' using 2:5 with lines smooth bezier title 'Sensor 3'

